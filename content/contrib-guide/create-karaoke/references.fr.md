+++
title = "Références"
weight = 9
+++

Cette section n'est utile que si vous souhaitez remplir consciencieusement toutes les données d'un karaoké. Ce n'est pas obligatoire pour le bon fonctionnement de Karaoke Mugen.

## Sites web de référence

Quand vous éditez/ajoutez un karaoké, il est important d'avoir de bonnes références pour ne pas se tromper sur les noms des artistes / chanteurs / compositeurs / studio / etc.
Attention, certains d'entre eux utilisent une graphie particulière (avec des apostrophes ou des caractères spéciaux).

{{% include "includes/inc_series-names.fr.md" %}}

## Paroles

{{% include "includes/inc_lyrics.fr.md" %}}

