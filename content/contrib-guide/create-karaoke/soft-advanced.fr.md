+++
title = "Autres tâches avancées"
weight = 7
+++

Lors de la manipulation de karaokés vous pourrez potentiellement avoir besoin d'effectuer des tâches un peu pointues. Pour vous aider, nous en listons ici quelques unes.

{{% notice warning "Attention!" %}}
Si vous n'êtes pas à l'aise avec la ligne de commande, passez votre chemin.
{{% /notice %}}

## Outils à installer

Vous aurez besoin de quelques outils pour continuer.

### Bash

**Bash** est, pour simplifier, un invité de commande Windows mais en plus puissant, et possède un système de scripting plus avancé que le traditionnel invité de Windows (CMD.EXE).

Sous Linux ou macOS, Bash est votre invite de commande par défaut (dans la majorité des distributions Linux en tous cas, ça peut aussi être **zsh** mais ça reviendra peu ou prou au même).

Pour les utilisateurs Windows, vous pouvez [la télécharger par ici](https://gitforwindows.org/). Une fois le logiciel installé, il vous suffit de le lancer en vous rendant dans un dossier de votre choix via l'explorateur et en faisant un clic droit dans le dossier, puis de selectionner **Git Bash**.

Sous Windows 10, une alternative est [d'utiliser le sous-système Linux](https://docs.microsoft.com/fr-fr/windows/wsl/install-win10) qui vous fournit un bash et où vous pouvez installer ffmpeg facilement.

### ffmpeg

**ffmpeg** s'utilise exclusivement en ligne de commande (bien que des versions avec interface sont disponibles sur le net).

[Téléchargez la dernière version pour votre système sur le site officiel](https://ffmpeg.org). Une version numérotée et avec un *linking* statique fera l'affaire. Notez que si vous utilisez un système de packages vous trouverez facilement ffmpeg dessus.

- Pour Windows : [Chocolatey](http://chocolatey.org)
- Pour macOS : [Homebrew](http://brew.sh)
- Pour Linux : à voir selon votre distribution. A noter que parfois votre distribution peut avoir un version très ancienne de ffmpeg.

#### Exemple d'utilisation sous Windows

Lorsque vous aurez besoin d'appliquer une ligne de script à un ou plusieurs fichiers, il vous suffira de :

- Déposer le(s) fichier(s) dans ce dossier aux côtés de **ffmpeg.exe**
- Ouvrir **(Git) Bash** en faisant clic-droit dans le dossier puis **Git Bash Here**
- Taper la ligne de script ffmpeg voulue puis valider avec `entrée`

## Vidéo

### Changer le conteneur d'une vidéo (par exemple changer un .avi en .mp4)

```sh
ffmpeg -i "VotreVidéoOriginelle.avi" "out.mp4"
```

ffmpeg permet de changer le conteneur d'une vidéo facilement, mais vous pouvez l'utiliser aussi pour convertir vers un mp3

### Remplacer l'audio

Pour une raison ou une autre, il peut arriver de vouloir remplacer l'audio d'une vidéo. Par exemple, pour une version instrumentale, il est recommmandé de synchroniser l'instrumentale avec la vidéo d'origine. Un autre exemple serait de remplacer l'audio d'un Insert pour enlever les perturbations sonores. Finalement, il est possible que vous vouliez tout simplement remplacer l'audio pour une version de meilleure qualité. **Quand vous remplacez l'audio, il est recommandé de remplacer avec un fichier audio dans un format non-compressé comme le `.wav`, `.flac` ou `.alac`**

Pour remplacer l'audio sans affecter la qualité de la vidéo, voici des instructions utiles:

D'abord, vous aurez besoin d'un logiciel de traitement audio (DAW en anglais). Voici quelques options adéquates:

- [https://www.audacityteam.org/download/](Audacity) (Gratuit)
- [https://www.reaper.fm/](Reaper) (Période d'essai gratuit de 60 jours)

Ensuite, ouvrez votre logiciel de traitement audio préféré et ajoutez deux pistes audio avec la source à remplacer et l'audio de remplacement.

Gardez la source immobile et décalez la piste audio de remplacement jusqu'à ce que les deux pistes sonnent synchro.

Par la suite, retirez l'audio de la source et faîtes un rendu du projet. Par exemple des débits et codecs audio de 192kb/s aac pour du `.mp4` et de 320kb/s mp3 pour du `.mp3` sont des valeurs saines. N'oubliez pas que si votre source a un débit plus bas, ça ne sert à rien de l'augmenter artificiellement lors du rendu.

Finalement, vous devrez ajouter l'audio au média d'origine. Pour ce faire, ouvrez un invite de commandes dans le bon dossier et utilisez ffmpeg via une commande comme celle-ci

```sh
ffmpeg -i "le-média-d-origine.mp4" -i "votre-nouveau-rendu-audio.m4a" -map 0:v -map 1:a -c copy "fichier-de-sortie.mp4"  
```
Pour un peu plus de détails sur les différentes instructions dans cette commande:

- `-i "le-média-d-origine.mp4"`: ajoute `le-média-d-origine.mp4` comme première entrée.
- `-i "votre-nouveau-rendu-audio.m4a"`: ajoute `votre-nouveau-rendu-audio.m4a` comme deuxième entrée.
- `-map 0:v`: utilise la vidéo de la première entrée.
- `-map 1:a`: utilise l'audio de la deuxième entrée.
- `-c copy`: ne pas réencoder (copier les flux des entrées). Vous pourriez également utiliser des instructions plus complexes comme `-c:v copy -c:a aac -b:a 192k` pour faire le réencodage à cette étape plutôt que dans votre logiciel de traitement audio.
- `"fichier-de-sortie.mp4"`: prendre le résultat de l'encodage et le mettre dans un fichier nommé `fichier-de-sortie.mp4`.

### Faire une conversion de masse (convertir des webm en mp4 par exemple)

```sh
ls *.webm >webm.txt
```

puis

```sh
while read file ; do f=`basename "$file" .webm`; ./ffmpeg -i "$file" "$f.mp4" ; done < webm.txt
```

### Extraire les sous-titres d'une vidéo mkv

```sh
ffmpeg -i "VotreVidéo.mkv" "out.ass"
```

### Extraire les sous-titres d'un batch de vidéos mkv

```sh
ls *.mkv >mkv.txt
```

puis

```sh
while read file; do f=`basename "$file" .mkv`; ./ffmpeg.exe -i "$file" "$f.ass" ; done < mkv.txt
```

### Corriger la durée

Il faut ré-encapsuler la vidéo comme suit :

```sh
ffmpeg -i video_source.mp4 -vcodec copy -acodec copy video_destination.mp4
```

### Corriger le problème de ratio

Une vidéo peut s'afficher correctement, mais les karaokés sont étirés ou des lettres sont ratées lors de la progression. C'est généralement un problème de ratio.

ffmpeg ou ffprobe indiquent ce ratio, le SAR (Sample Aspect Ratio).

Par exemple

```
ffprobe kochoneizer.mp4
Input #0, mov,mp4,m4a,3gp,3g2,mj2, from 'kochoneizer.mp4':
Metadata:
    major_brand     : isom
    minor_version   : 512
    compatible_brands: isomiso2avc1mp41
    encoder         : Lavf58.76.100
Duration: 00:01:31.76, start: 0.000000, bitrate: 1843 kb/s
Stream #0:0(und): Video: h264 (High) (avc1 / 0x31637661), yuv420p(tv, smpte170m/bt470bg/bt470bg), 720x576 [SAR 64:45 DAR 16:9], 1710 kb/s, 25 fps, 25 tbr, 12800 tbn, 50 tbc (default)
    Metadata:
    handler_name    : VideoHandler
    vendor_id       : [0][0][0][0]
Stream #0:1(und): Audio: aac (LC) (mp4a / 0x6134706D), 48000 Hz, stereo, fltp, 127 kb/s (default)
    Metadata:
    handler_name    : SoundHandler
    vendor_id       : [0][0][0][0]
```

L'élément à regarder est `720x576 [SAR 64:45 DAR 16:9]`

Ici, le SAR est défini à 64:45, pour une résolution de 720×576. Le DAR (Display Aspect Ratio) indique le format (ici, vidéo en 16:9).

Le paramètre SAR indique la forme d'un pixel. Contrairement à ce que l'on pourrait croire, en vidéo, un pixel n'est pas forcément carré.

Un pixel peut être carré (SAR à 1:1), ou rectangulaire (par exemple 64:45, mais cette valeur peut être différente).

![ass](/images/creation/SAR-11.png)

Ici, le SAR est défini à 1:1. Les pixels sont carrés

![ass](/images/creation/SAR-64-45.png)

Ici, le SAR est défini à 64:45. Les pixels sont rectangulaires.

Pourquoi utiliser ce paramètre SAR ? Pour diverses raisons.

- Utilisation du format 16:9 en étirant les pixels en largeur ; pratique extrêmement répandue avec les DVD vidéo.
- Réduire l'utilisation de la bande passante lors de la diffusion TV (DVB-T/TNT), où pendant un moment (peut-être encore aujourd'hui), la vidéo est diffusée en 1440×1080 et étirée (avec le SAR) en 1920×1080.

Pour calculer la résolution de sortie, c'est simple :

```
largeur × SAR.
```

Donc 720 × (64 ÷ 45), ce qui donne 1024.

Et si on divise 1024 par 576, on retrouve 16/9 (16:9), la valeur du DAR.

Pour corriger le ratio de la vidéo, il faudra la réencoder en utilisant le paramètre vf scale de ffmpeg et en utilisant la largeur calculée plus haut :

```sh
ffmpeg -i kochoneizer.mp4 -c:a copy -c:v libx264 -vf "scale=1024:576" kochoneizer-1024.mp4
```

{{% notice note "Note" %}}
SAR est une appellation dans ffmpeg. Dans d'autres logiciels, on parle de PAR (Pixel Aspect Ratio). C'est exactement la même chose.
{{% /notice %}}
