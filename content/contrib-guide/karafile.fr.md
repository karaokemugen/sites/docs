+++
title = "Les fichiers karaoké"
weight = 4
+++

Les informations d'un karaoké sont stockées dans un fichier de métadonnées. Il est primordial : il indique à Karaoke Mugen où sont les fichiers à utiliser ainsi que d'autres infos pour générer sa base de données.

On y trouve aussi toutes les métadonnées concernant votre karaoké : le nom du chanteur, du parolier, du studio ayant réalisé la vidéo, la date de création ou la langue utilisée pour chanter.

Ici, nous allons voir comment créer ou modifier ce fichier via une interface graphique.

## Accès à l'interface

- L'interface de gestion des karaokés se trouve dans Karaoke Mugen. Lancez le programme et sur l'écran d'accueil, cliquez sur le bouton **Panneau Système** (vous pouvez aussi y accéder via l'onglet en haut de la fenêtre **Allez à** > **Panneau Système**).

![ass](/images/creation/System-FR.jpg)

- Une fois dans la partie système, cliquez sur le 3e onglet **Karaokés** puis cliquez sur **Nouveau** pour accéder au formulaire de création de karaoké.

![ass](/images/creation/SystemTab-FR.png)

## Remplir les informations du karaoké

Vous voilà donc dans le formulaire de création de kara.

![ass](/images/creation/Karacreatepage-FR.png)

Les informations contenus dans le fichier karaoké sont écrits sous la forme de tags. Les tags ont leur propre [fichier de données](../tag/), qui contient les éventuelles variations régionales et alias.

Soit vous avez une liste de cases à cocher (auquel cas vous pouvez cocher les cases appropriées), soit vous avez un champ avec auto-complétion qui vous proposera soit de cliquer sur un tag existant, soit de créer un tag directement.

Si vous êtes amené à créer des tags, vous pourrez, après la création de votre karaoké, éditer lesdits tags pour ajouter des informations supplémentaires.

**Passons en revue le formulaire pour mieux le comprendre :** 

### Fichier vidéo

C'est votre source vidéo (ou audio si vous avez timé un mp3). Cliquez sur le bouton et allez chercher votre fichier pour l'ajouter (vous pouvez aussi le drag & drop directement).

### Fichier de sous-titres

C'est votre *time*, votre fichier *ASS*. Cliquez sur le bouton et allez chercher votre fichier pour l'ajouter (vous pouvez aussi le drag & drop directement).

### Karaokés parents

Ce concept de kara parents / enfants sert à regrouper les karaokés portants sur la même chanson, sans pour autant que ces derniers ne proviennent de la même source vidéo. Voilà un exemple pour mieux comprendre :  

Le kara **JPN - Shin Seiki Evangelion - OP - Zankoku na Tenshi no These** est un kara parent, et tous les autres karas de cette chanson sont ses enfants, que ça soit les reprises, les AMV, les concerts, etc.  
  
Dans cette section, en selectionnant un karaoké parent déjà existant, non seulement cela copie les informations du parent sur votre karaoké que vous être en train de créer, mais le définit également comme "enfant" du karaoké parent. Cela veut dire que les utilisateurs verront que le parent a différentes versions enfant. Il peut s'agir de toutes les versions différentes de l'opening de Sakura Wars, ou bien encore une *music video* qui a comme parent un *opening*.

En sélectionnant le parent, les visiteurs peuvent plus facilement découvrir qu'il existe des versions alternatives.

Un karaoké peut avoir plusieurs parents.

### Titre du morceau

C'est le titre de la chanson de votre karaoké. Il peut exister en plusieurs langues et avoir des alias. Les alias servent au moteur de recherche et vous pouvez indiquer des mots qui ne se trouvent pas déjà dans le(s) titre(s) du karaoké.

Vous pouvez ajouter plusieurs titres pour un même karaoké. 

Avant d'ajouter un titre, vous devez d'abord indiquer dans quelle langue vous allez l'ajouter.

Dans le cas d'un karaoké d'animé par exemple, ajoutez la langue "Japonais (JPN)" et renseignez le titre **en caractère japonais (katakana ou hiragana)**. Vous pouvez également ajouter sa retranscription en romaji dans la langue "Alphabet Latin (Romanisation) (QRO)".  

Enfin, sélectionnez la langue par défaut du karaoké, à savoir la langue de son pays d'origine. 

### Alias

En complément des titres, vous pouvez ajouter des alias. Ils ne s'afficheront pas dans l'application mais seront quand même pris en compte lors d'une recherche. **Ajoutez des alias qui sont utiles** et qui contiennent des mots qui ne sont pas dans les titres renseignés plus haut. 

Exemple : vous avez rajouté le kara **Ai Kotoba Ⅳ** avec **Love Words IV** en anglais. Vous pouvez mettre un simple `4` en alias. Cela suffira à KM pour trouver le kara si vous cherchez "Love Words 4"

Vous pouvez mettre un simple `4` en alias car les mots "Ai", "Kotoba", "Love" et "Words" sont déjà pris en compte par le moteur de recherche (puisqu'ils sont dans les titres), inutile de les rajouter une seconde fois.

### Langues(s)

La langue dans laquelle votre karaoké est interprêté. Une liste existe déjà, il suffit de commencer à taper la langue et de sélectionner l'un des résultats qui ressort.

- Si c'est une langue inconnue / inventée, ajoutez la langue "Langue inconnue".
- Si votre karaoké ne contient aucun chant de base (et donc que votre fichier de sous-titres est vide), ajoutez la langue "Pas de contenu linguistique".
- Si vous avez timé une version *instrumentale* (donc sans parole), indiquez tout de même la langue dans laquelle le kara sera chanté par le public.

### Œuvre(s)

Dans le cas d'une chanson faisant partie d'une oeuvre, comme une série TV ou un film par exemple.

C'est ici que vous ajoutez l'œuvre dont est issue votre karaoké (ou plusieurs si vous faites des AMV par exemple).

Cliquez sur **Ajout** et commencez à taper son titre, vous verrez alors des suggestion de séries déjà présentes dans la base.

![ass](/images/creation/KaracreatepageSerie-FR.png)

Si vous trouvez votre série dans la liste, cliquez dessus et elle s'ajoutera au formulaire. S'il s'avère que votre série n'existe pas encore, renseignez-la (son nom d'origine) et ajoutez-là avec le bouton bleu `Créer un tag`, un tag va être créé automatiquement avec ce nom. Vous pourrez ensuite apporter des informations supplémentaires à ce tag dans l'onglet dédié aux tags de la base.

### Franchise(s)

Ce champ est à remplir si vous ajoutez un karaoké lié à une franchise globale.  

Exemple :  Si vous ajoutez l'ending de l'œuvre **Sound! Euphonium saison 2**, alors il faut lier le kara à la franchise **Sound! Euphonium**

### Types de chanson

C'est ici que vous indiquez dans quel contexte la vidéo a été crée. Vous avez le choix entre : Générique de début, générique de fin, insert song, Music Video, AMV, vidéo promotionnelle (PV), pub (CM), concert, character song, image song, audio uniquement, replay de stream ou divers / inclassable

### Numéro de chanson

Les numéros de chanson n'affectent en général que les chansons issues d'animés japonais.

### Versions

Les tags *Versions* vous permettent de préciser une altération d'une chanson, pratique pour les cas où plusieurs karaokés représentent la même chanson, mais que c'est une reprise, une version instrumentale, etc.

Lorsque vous allez créer votre karaoké, les versions vont être disponibles sous forme de cases à cocher (vous pouvez en cocher plusieurs). Si vous devez préciser une variation ne figurant pas dans la liste (comme par exemple une variation liée à un épisode dans la série ou la même chanson mais chantée par un autre personnage), vous pouvez le préciser après le titre de la chanson en adoptant la forme `<titre> ~ <version> Vers.` par exemple.  

### Chanteur(s)

Le(s) interprète(s) de votre karaoké.  

### Groupe musical

Cette section facultative sert à indiquer le nom du groupe qui interprète la chanson. Il peut s'agir d'un vrai groupe (BTS, ASIAN KUNG-FU GENERATION, JAM Project) ou d'un groupe "fictif" crée pour une œuvre spécifique (Kessoku Band, µ's, Hô-kago Tea Time, Nightcord at 25:00, hololive IDOL PROJECT)

### Compositeurs(s)

Le(s) auteur(s), compositeur(s), arrangeur(s) **et** parolier(s) de votre karaoké. Comme les œuvres, une liste existe déjà, vérifiez que vos compositeurs ne sont pas déjà dedans pour ne pas créer un doublon.  

### Créateur(s)

L'entité qui a crée la vidéo (souvent studio d'animation/jeu)

### Année de diffusion

Tout est dans le titre.

### Autres tags

- **Collection(s)** : A quelle collection le karaoké appartient ? Selon sa provenance, sa langue, son type, il ira dans une collection mais pas dans une autre. Les utilisateurs pourraient ne pas voir le karaoké s'il est dans une collection qu'ils ont cachée.
- **Famille(s)** : Type de famille du karaoké.	
- **Plateforme(s)** : Pour les kara de jeux vidéo, sur quelle(s) plateforme(s) le jeu est-il sorti ?
- **Thèmes** : Quel genre d'œuvre est-ce ?
- **Origine(s)** : D'où vient le karaoké ?
- **Divers** : Tout ce qui ne rentre pas ailleurs.
- **Groupe(s)** : Permet de regrouper plusieurs kara dans des "familles". Même en ne rentrant rien, KM ajoutera automatiquement un tag indiquant la décennie d'où vient votre karaoké.
- **Warning(s)** : Permet d'indiquer que ce karaoké peut potentiellement poser des problèmes à un public (par exemple des flashs rapides, spoiler une œuvre ou du contenu pour adulte).

### Auteur(s) du karaoké

C'est vous ! Tapez votre nom ou pseudo pour vous ajouter dans la liste des "Karaoke Maker".

### Dépôt

Dans quel dépôt souhaitez-vous ajouter ce kara ? Vous pouvez choisir de le mettre dans votre base locale, ou bien dans une base que vous partagez avec d'autres personnes.

### Validation et création du fichier karaoké

Une fois que tous les champs sont remplis, il ne vous reste plus qu'à cliquer sur le gros bouton bleu  `Sauvegarder` pour générer le fichier de votre kara.

Si tout s'est bien passé, vous devriez voir apparaitre une petite notification en haut de l'appli.

Le fichier kara que vous venez de créer a été placé dans votre dossier primaire "karaokes", et la vidéo et le fichier *.ass* ont été renommés selon ce que vous avez indiqué dans le formulaire, et placés respectivement dans les dossiers "medias" et "lyrics" (ou dans le premier d'entre eux si vous en avez indiqué plusieurs dans votre fichier `config.yml`).

Pour les retrouver parmi tous les autres fichiers, triez-les par date de modification dans votre explorateur de fichiers.

Aussi, votre nouveau karaoké a été ajouté à votre base de données sans que vous ayez à régénérer celle-ci, ce qui vous permet de tester tout de suite votre ajout.

Si vous souhaitez modifier un fichier kara, [rendez-vous sur cette page.](../create-karaoke/editkaraoke#modifier-les-informations-dun-karaoké-ou-dune-série)
