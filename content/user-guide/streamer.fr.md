+++
title = "Guide du streameur"
weight = 9
+++

Vous avez une chaîne Twitch et vous aimez y faire du karaoké. Karaoke Mugen est peut-être fait pour vous (on l'espère) !

Le logiciel possède plusieurs options pour faciliter votre stream de karaoké. Voici ce que vous devez savoir pour commencer.

## Téléchargez et installez Karaoke Mugen

Pour cela, [consultez la section Installation de la documentation](../install).

Lors de son premier lancement, Karaoke Mugen va télécharger l'intégralité de la base de données, mais sans les médias.

Pour plus d'informations sur comment utiliser Karaoke Mugen en général, [consultez la section concernée](../getting-started)

## Au sujet des médias

L'intégralité des médias de Karaoke Mugen dépasse plusieurs centaines de giga-octets, mais vous n'avez pas besoin de tout télécharger.

Nous vous recommandons néanmoins de pré-télécharger tous les karaokés via le panneau système si vous avez suffisament de stockage disponible, afin d'éviter de surcharger votre connexion internet ou le serveur de Karaoke Mugen pendant votre stream.

Si vous ne pouvez ou ne voulez pas télécharger les médias à l'avance, pas de panique : Karaoke Mugen les téléchargera au fur et à mesure de votre session de karaoké de la façon suivante :

- Si vous jouez une chanson directement depuis la bibliothèque, elle ne sera pas téléchargée mais elle sera streamée depuis le serveur de Karaoke Mugen.
- Si vous ajoutez une chanson à la liste de lecture courante (celle utilisée par le lecteur vidéo), elle sera téléchargée en arrière-plan.

Si vous êtes à court d'espace disque, Karaoke Mugen fera du streaming depuis le serveur. Vous pouvez supprimer des médias téléchargés depuis le panneau système pour faire de la place.

## Au sujet du dossier de données

Quand on parle du **dossier de données**, il s'agit généralement de là où Karaoke Mugen met ses fichiers.

Sous Windows, par défaut il se trouve dans `C:\Utilisateurs\votreUtilisateur\KaraokeMugen`

Sous macOS ou Linux, il s'agit du dossier `~/KaraokeMugen` (il se trouve à la racine de votre dossier utilisateur)

## Préparez votre layout

### Le lecteur vidéo

Celui-ci diffusera la chanson ainsi que les paroles. Vous pouvez agrandir/réduire la fenêtre comme vous l'entendez et l'ajouter à votre layout sous OBS/Streamlabs

Vous pouvez le passer en plein écran dans les options, ou cliquer dessus et appuyer sur la touche F de votre clavier pour obtenir le même effet. Rappuyer sur F ensuite repasse le lecteur en fenêtré.

### La fenêtre de liste de lecture

Une fenêtre de liste de lecture peut être activée depuis la barre de menus de l'application, dans le menu **Fenêtre**. Capturez cette fenêtre pour afficher la chanson en cours, une barre de progression, ainsi que les chansons à suivre. Vous pouvez redimensionner la fenêtre comme bon vous semble pour coller à votre layout.

### Les fichiers texte d'information

Karaoke Mugen écrit des informations sur le karaoké en cours dans des fichiers textes, que vous pouvez surveiller avec OBS pour afficher ces informations comme bon vous semble sur votre layout.

Les fichiers sont dans le dossier `streamFiles` du dossier de données. Voici le contenu des fichiers :

- `song_name.txt` : Titre de la chanson en cours
- `requester.txt` : Le nom de la personne qui a demandé la chanson
- `km_url.txt` : l'URL d'accès à votre karaoké (`http://abcd.kmserver.net`)
- `frontend_state.txt` : Etat de l'interface (Ouverte, Fermée, Limitée)
- `current_kara_count.txt` : Nombre de karaokés dans la liste de lecture courante
- `public_kara_count.txt` : Nombre de karaokés dans la liste de lecture publique
- `time_remaining_in_current_playlist.txt` : Temps restant dans la liste de lecture courante au format `1h20m` par exemple.
- `next_song_name.txt` : Nom de la prochaine chanson.
- `next_requester.txt` : Nom de la personne ayant demandé la chanson à suivre.

## Personnalisez Karaoke Mugen

### Le fond d'écran

Le fond d'écran par défaut peut être changé via le panneau système, dans la page "Fonds d'écran". Un fond d'écran aléatoire sera chargé à chaque pause s'il y a plusieurs fonds disponibles.

### La musique des pauses

Par défaut il n'y a aucune musique de fond fournie avec Karaoke Mugen.

Vous pouvez néanmoins en ajouter vous-même via la page "Fonds d'écran" du panneau système en déposant des fichiers musicaux (mp3, m4a, ogg, flac, wav...) dedans. Ils seront lus durant les pauses entre deux chansons.

Si une musique porte le même nom qu'un fichier image (sans l'extension) celle-ci sera lue lorsque ce fichier image sera sélectionné au hasard. Sinon, une musique aléatoire sera choisie.

## Configurez Karaoke Mugen

On entre dans le vif du sujet.

Tout dépend de ce que vous souhaitez faire avec votre session de karaoké.

### Vous voulez garder le contrôle sur votre karaoké

Pas de problème, vous pouvez vous préparer une liste de lecture à l'avance et la jouer, ou demander à votre chat Twitch de taper des noms de chansons que vous sélectionnerez ensuite vous-même, à l'ancienne.

### Faire participer vos viewers

#### Accès à votre karaoké

Avant toute chose, assurez-vous que **l'accès à distance** est actif (il l'est par défaut) et qu'il a réussi à se connecter. Sinon, vos viewers ne pourront pas se connecter à votre interface Karaoke Mugen via Internet.

Si vous voyez l'URL d'accès dans le lecteur vidéo de type `https://xxxx.kmserver.net` alors c'est bon. Sinon, assurez-vous d'avoir la dernière version de Karaoke Mugen et que celui-ci n'est pas bloqué par votre pare-feu ou votre antivirus. Si le problème persiste, [contactez-nous](https://mugen.karaokes.moe/contact.html).

L'adresse indiquée sera celle à communiquer à vos viewers, via votre layout par exemple, ou un bot que vous aurez configuré au préalable.

Les quatre caractères au début de l'URL changeront si vous n'utilisez pas Karaoke Mugen pendant plus de 15 jours. Si vous voulez une adresse personnalisée et permanente, [il y a un tier pour ça sur notre Patreon](https://patreon.com/karaokemugen) aussi.

#### Options

##### Stream

Dans le menu des options de Karaoke Mugen (interface opérateur > menu K en haut à droite > options) allez dans l'onglet **Karaoké**

- Activez le **Mode Stream**
- Réglez le **temps de pause** à une valeur qui vous convient (30 secondes, par exemple). Si jamais vous avez besoin de vous absenter lors d'une pause, appuyez sur le bouton **Stop** du lecteur pour mettre en pause la pause (si si.)
- Activez **le chat Twitch**. Pour cela vous aurez besoin de votre *jeton OAuth*. Un lien dans le panneau d'options vous permettra de le récupérer. Il permettra à Karaoke Mugen de lire et d'écrire sur votre chat Twitch, comme un bot.

Si le chat Twitch est activé, Karaoke Mugen répondra aux commandes suivantes :

- `!vote` : voir ci-dessous
- `!song` : affiche la chanson en cours.

##### Live Comments

Si vous avez renseigné votre Jeton OAuth de Twitch dans les options comme indiqué ci-dessus, vous pouvez activer les "Live Comments" dans le menu de réglages rapides en haut à gauche de l'interface opérateur.

Lorsque les gens parleront sur votre chat Twitch, leurs messages apparaîtront sur votre lecteur vidéo en défilant, comme sur le site Nico Nico Douga.

##### Intermissions

Selon vos préférences vous voudrez peut-être désactiver les jingles entre chansons, les encore, outros, sponsors et intros. Mais laissez-lez, ils sont rigolos et des gens sont morts pour les réaliser. Presque.

##### Limites

Afin de limiter les abus vous pouvez définir combien de chansons vos viewers peuvent proposer à la fois. Une chanson est "libérée" et le quota rendu lorsqu'elle passe à l'écran ou qu'elle est ajoutée à la liste de lecture courante (selon ce qui arrive en premier)

Le quota peut être :

- **Désactivé** : les viewers peuvent proposer autant de chansons qu'ils veulent mais ça peut être long pour vous ou votre opérateur de karaoké de choisir
- **Par nombre de chansons** : par exemple, 2 ou 3 chansons maximum à la fois
- **Par temps** : pour éviter que quelqu'un ne propose que des chansons longues, un temps de quota en secondes est configurable.

#### Listes de lecture

En règle générale vous voudrez créer une nouvelle liste de lecture "Suggestions" et lui donner la propriété "Publique". C'est vers cette liste qu'iront les suggestions de vos viewers automatiquement.

En affichant d'un côté la liste publique et de l'autre la liste courante, vous pourrez refuser ou accepter des chansons.

- Les refuser : elles restent dans la liste pour ne pas être reproposées de nouveau, et libèrent le quota de la personne qui a proposé.
- Les accepter : elles sont envoyées dans la liste courante pour que le lecteur les joue et cela les libère aussi.

Soit vous les acceptez/refusez depuis la liste, soit vous pouvez les passer tous en revue facilement en cliquant sur l'un des karaokés. Une fois sur sa fiche, il y aura des boutons pour accepter/refuser une chanson, et ça passera ensuite à la suivante.

#### Liste noire

La liste noire permet d'éviter que des chansons ne soient sélectionnables, si vous tenez absolument à ne jamais voir certains chanteurs, certaines séries ou des karaokés non montrables.

Quelques exemples :

- Si vous n'aimez pas les chansons de mecha, vous pouvez bannir le thème "Mecha"
- Les chansons avec le tag "R18", "Spoiler" ou "Epilepsie".

Ces critères sont modifiables dans la vue "Critères" dans la liste de lecture qui est définie comme [liste noire](../playlists). Vous pouvez avoir plusieurs listes noires selon vos envies.

#### Sondages

Une autre façon populaire de faire participer vos viewers est d'activer le mode sondage. Comment ça marche ?

- Remplissez la liste de suggestions (publique) des karaokés que vous aimez chanter.
- Mettez un ou deux de ces karaokés dans la liste de lecture courante.
- Activez le mode sondage dans les options de Karaoke Mugen (onglet Karaoke)
- Définissez un temps de sondage **inférieur** au temps de pause que vous avez défini dans le mode stream, ceci afin que les gens aient le temps de voir le résultat du sondage.

Lorsque vous commencerez à chanter, durant chaque pause les viewers pourront voter parmi N choix de la liste de suggestions. Ils seront affichés dans le chat Twitch et a l'écran.

Plusieurs façons de voter :

- Via le chat Twitch avec la commande `!vote n` où `n` est le numéro de la chanson affichée
- Via l'interface web si vous leur avez laissé l'accès.

## Au moment de streamer

Ca y est, c'est le grand moment.

Démarrez Karaoke Mugen, changez votre layout, vous êtes paré(e) !

### Nommez votre session

Sur l'écran d'acceuil, en haut à droite, vous pouvez changer la session de karaoké en cours et la renommer. Cela permet de mieux identifier les sessions entre elles et de les retrouver (pour des stats par exemple !)

Vous pouvez également rendre des sessiosn privées (dont les statistiques ne seront pas comptabilisées)

### Ouvrir l'interface à vos viewers

Pensez à sélectionner le niveau d'ouverture de l'interface pour vos viewers :

- **Fermé** : Une page les invitera à patienter
- **Restreint** : Les viewers ne pourront que consulter la chanson en cours, les paroles et la liste de lecture courante
- **Ouverte** : Les viewers pourront proposer des chansons à la liste de suggestions.

Passez en mode **Restreint** vers la fin de votre karaoké, quand vous êtes sûr(e) que vous ne voudrez plus aucune suggestion.

### Nommez un ou plusieurs co-opérateurs

Une fois vos fidèles modérateurs inscrits sur votre session de karaoké, vous pourrez les nommer "opérateurs" eux aussi pour qu'ils puissent vous aider à gérer votre karaoké, par exemple modifier la playlist en temps réel, accepter ou refuser des chansons selon vos instructions...

Pour faire cela, une fois leur compte crée, allez dans le panneau système et dans la section liste des utilisateurs. Trouvez vos modérateurs et modifiez leur compte pour les passer opérateur !

### C'est parti !

Vous voilà star du karaoké !
